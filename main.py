#! /usr/bin/env/ python3
# coding: utf-8


from mesh_gen import MeshGenMeshPy

def main():
    ref_point = [0.0, 0.0, 0.0]
    size = [1.0, 1.0, 1.0]
    dims = [20, 20, 20]
    msh_gen = MeshGenMeshPy()
    msh_gen.make_save_mesh(ref_point, size, dims, 'cube_meshpy.vtk')
    pass


if __name__=='__main__':
    main()

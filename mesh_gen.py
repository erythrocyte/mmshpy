# ! /usr/bin/env python3
# coding: utf-8

from meshpy.tet import MeshInfo, build
import itertools
import numpy as np

class MeshGenMeshPy:
    def make_save_mesh(self, p0, size, dims, mesh_name, need_print=False):
        """
        """
        mesh_info = MeshInfo()

        points = self.make_points_3d(mesh_info, p0, size, dims)
        mesh_info.set_points(points)

        facets = self.make_bound_faces(mesh_info, dims)
        mesh_info.set_facets(facets)

        # print(points)
        # p = self.get_corner_points(dims)
        # print(p)
        # for pi in p:
            # print(points[pi])

        # for face in facets:
            # s = ''
            # for pi in face:
                # s = s + str(points[pi]) + ' '
            # print(s)

        print('Mesh building started')
        mesh = build(mesh_info)
        print('Mesh building completed')

        if (need_print is True):
            print("Mesh Points:")
            for i, p in enumerate(mesh.points):
                print(i, p)

            print("Point numbers in tetrahedra:")
            for i, t in enumerate(mesh.elements):
                print(i, t)

        print('Mesh as vtk saving started')
        mesh.write_vtk(mesh_name)
        print('Mesh as vtk saving completed')

    def make_points_3d(self, mesh_info, p0, size, dims):
        """
        """
        [x0, y0, z0] = p0
        [lx, ly, lz] = size
        [xl, yl, zl] = [x0 + lx, y0 + ly, z0 + lz]
        [nx, ny, nz] = dims
        [dx, dy, dz] = [lx / nx, ly / ny, lz / nz]

        points = [
            (x0, y0, z0), (xl, y0, z0), (xl, yl, z0), (x0, yl, z0),
            (x0, y0, zl), (xl, y0, zl), (xl, yl, zl), (x0, yl, zl),
        ]

        x = np.linspace(x0 + dx, lx - dx, nx)
        y = np.linspace(y0 + dy, ly - dy, ny)
        z = np.linspace(z0 + dz, lz - dz, nz)

        lxyz = [tuple(d) for d in itertools.product(x, y, z)]

        for xyz in lxyz:
            points.append(xyz)

        return points

    def make_bound_faces(self, mesh_info, dims):
        """
        """
        p = self.get_corner_points(dims)
        [p0, p1, p2, p3, p4, p5, p6, p7] = p

        facets = [
            [0,1,2,3],
            [4,5,6,7],
            [0,4,5,1],
            [1,5,6,2],
            [2,6,7,3],
            [3,7,4,0],
        ]

        # facets = [
            # [p0, p4, p6, p2],
            # [p1, p5, p7, p3],
            # [p0, p1, p5, p4],
            # [p4, p5, p7, p6],
            # [p6, p7, p3, p2],
            # [p2, p3, p1, p0],
        # ]

        return facets

    def get_corner_points(self, dims):
        """
        """
        [nx, ny, nz] = dims
        p0 = 0
        p1 = p0 + (nz - 1)
        p2 = p1 + (ny - 2) * nz + 1
        p3 = p2 + (nz - 1)

        p4 = (nz * ny) * (nx - 1)
        p5 = p4 + (nz - 1)
        p6 = p5 + (ny - 2) * nz + 1
        p7 = p6 + (nz - 1)

        p = [p0, p1, p2, p3, p4, p5, p6, p7]

        return p
